package br.com.maknamara.vaccinecontrol.controllers;

import java.time.LocalDate;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.maknamara.vaccinecontrol.model.User;
import br.com.maknamara.vaccinecontrol.model.Vaccination;
import br.com.maknamara.vaccinecontrol.service.VaccinationService;

@AutoConfigureMockMvc
@WebMvcTest(VaccinationRegistrationControll.class)
public class VaccinationRegistrationControllTest {

    @MockBean
    private VaccinationService vaccinationService;

    @Autowired
    private MockMvc mockMvc;

    private static ObjectMapper mapper;

    @BeforeAll
    public static void beforeAll() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        /*
         * ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
         * validator = factory.getValidator();
         */
    }

    @Test
    public void saveVaccinationInvalidValues() throws Exception {
        Vaccination vaccination = new Vaccination();
        vaccination.setVaccinationDate(LocalDate.now());
        User user = new User();
        user.setId(1L);
        vaccination.setId(1L);

        Mockito.when(vaccinationService.save(Mockito.any())).thenReturn(vaccination);

        String jsonVaccination = VaccinationRegistrationControllTest.mapper.writeValueAsString(vaccination);

        String result = "{\"name\":\"O campo \\\"Nome\\\" Ã© obrigatÃ³rio\",\"user\":\"O campo \\\"UsuÃ¡rio\\\" Ã© obrigatÃ³rio\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/save-vaccination").content(jsonVaccination)
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().string(result));
    }

    @Test
    public void deleteVaccination() throws Exception {
        Mockito.doNothing().when(vaccinationService).deleteById(Mockito.any());
        mockMvc.perform(MockMvcRequestBuilders.delete("/delete-vaccination/{id}", "11").param("id", "1")
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(
                        MockMvcResultMatchers.content().string("{\"message\":\"VacinaÃ§Ã£o excluÃ­da com sucesso\"}"));
    }

    @Test
    public void saveVaccinationByPOST() throws Exception {

        Vaccination vaccination = new Vaccination();
        vaccination.setVaccinationDate(LocalDate.now());
        vaccination.setName("xpto");
        User user = new User();
        user.setId(1L);
        vaccination.setUser(user);
        vaccination.setId(1L);

        Mockito.when(vaccinationService.save(Mockito.any())).thenReturn(vaccination);

        String jsonVaccination = VaccinationRegistrationControllTest.mapper.writeValueAsString(vaccination);

        mockMvc.perform(MockMvcRequestBuilders.post("/save-vaccination").content(jsonVaccination)
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("{\"message\":\"VacinaÃ§Ã£o salva com sucesso\"}")));
    }

    @Test
    public void saveVaccinationByPUT() throws Exception {

        Vaccination vaccination = new Vaccination();
        vaccination.setVaccinationDate(LocalDate.now());
        vaccination.setName("xpto");
        User user = new User();
        user.setId(1L);
        vaccination.setUser(user);
        vaccination.setId(1L);
        vaccination.setId(1L);

        Mockito.when(vaccinationService.save(Mockito.any())).thenReturn(vaccination);

        String jsonVaccination = VaccinationRegistrationControllTest.mapper.writeValueAsString(vaccination);

        mockMvc.perform(MockMvcRequestBuilders.put("/update-vaccination").content(jsonVaccination)
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("{\"message\":\"VacinaÃ§Ã£o atualizada com sucesso\"}")));
    }

    @Test
    public void listVaccinationsEmpty() throws Exception {

        List<Vaccination> list = List.of();

        Mockito.when(vaccinationService.listAllVaccinationsOrderedByNameAscending()).thenReturn(list);

        String result = VaccinationRegistrationControllTest.mapper.writeValueAsString(list);

        mockMvc.perform(MockMvcRequestBuilders.get("/list-vaccinations")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(result)));
    }

    @Test
    public void listVaccinations() throws Exception {

        Vaccination vaccination = new Vaccination();
        vaccination.setVaccinationDate(LocalDate.now());
        vaccination.setName("xpto");
        User user = new User();
        user.setId(1L);
        vaccination.setUser(user);
        vaccination.setId(1L);

        List<Vaccination> list = List.of(vaccination);

        Mockito.when(vaccinationService.listAllVaccinationsOrderedByNameAscending()).thenReturn(list);

        String result = VaccinationRegistrationControllTest.mapper.writeValueAsString(list);

        mockMvc.perform(MockMvcRequestBuilders.get("/list-vaccinations")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(result)));
    }
}