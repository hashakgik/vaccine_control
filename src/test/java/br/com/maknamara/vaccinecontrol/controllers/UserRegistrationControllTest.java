package br.com.maknamara.vaccinecontrol.controllers;

import static org.mockito.Mockito.times;

import java.time.LocalDate;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.maknamara.vaccinecontrol.exceptions.DeletionNotAllowedException;
import br.com.maknamara.vaccinecontrol.exceptions.RecordsConflictException;
import br.com.maknamara.vaccinecontrol.model.User;
import br.com.maknamara.vaccinecontrol.service.UserService;

@AutoConfigureMockMvc
@WebMvcTest(UserRegistrationControll.class)
public class UserRegistrationControllTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    private static ObjectMapper mapper;

    @BeforeAll
    public static void beforeAll() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        /*
         * ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
         * validator = factory.getValidator();
         */
    }

    @Test
    public void saveUserConflitValues() throws Exception {

        User user = new User();
        user.setBirthDate(LocalDate.now());
        user.setCpf("12365478965");
        user.setEmail("nome@site.com");
        user.setName("xpto");
        user.setId(1L);

        Mockito.when(userService.save(Mockito.any())).thenThrow(new RecordsConflictException());

        String jsonUser = UserRegistrationControllTest.mapper.writeValueAsString(user);

        String result = "{\"message\":\"Email e/ou cpf jÃ¡ cadastrado no sistema\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/save-user").content(jsonUser).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict()).andExpect(MockMvcResultMatchers.content().string(result));
    }

    @Test
    public void saveUserInvalidValues() throws Exception {
        User user = new User();
        user.setBirthDate(LocalDate.now());
        user.setCpf("123");
        // user.setEmail("nome@site.com");
        user.setName("xpto");
        user.setId(1L);

        Mockito.when(userService.save(Mockito.any())).thenReturn(user);

        String jsonUser = UserRegistrationControllTest.mapper.writeValueAsString(user);

        String result = "{\"cpf\":\"O tamanho do campo \\\"CPF\\\" Ã© de 11 digitos\",\"email\":\"O campo \\\"Email\\\" Ã© obrigatÃ³rio\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/save-user").content(jsonUser).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andExpect(MockMvcResultMatchers.content().string(result));
    }

    @Test
    public void deleteUserWithSuccess() throws Exception {
        Mockito.doNothing().when(userService).deleteById(Mockito.anyLong());
        mockMvc.perform(MockMvcRequestBuilders.delete("/delete-user/{id}", "1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string("{\"message\":\"UsuÃ¡rio excluÃ­do com sucesso\"}"));
        Mockito.verify(userService, times(1)).deleteById(Mockito.anyLong());
    }

    @Test
    public void deleteUserWithoutSuccess() throws Exception {
        Mockito.doThrow(new DeletionNotAllowedException()).when(userService).deleteById(Mockito.anyLong());
        mockMvc.perform(MockMvcRequestBuilders.delete("/delete-user/{id}", "1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed());
        Mockito.verify(userService, times(1)).deleteById(Mockito.anyLong());
    }

    @Test
    public void saveUserByPOST() throws Exception {

        User user = new User();
        user.setBirthDate(LocalDate.now());
        user.setCpf("12354896575");
        user.setEmail("nome@site.com");
        user.setName("xpto");

        Mockito.when(userService.save(Mockito.any())).thenReturn(user);

        String jsonUser = UserRegistrationControllTest.mapper.writeValueAsString(user);

        mockMvc.perform(MockMvcRequestBuilders.post("/save-user").content(jsonUser).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated()).andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("{\"message\":\"UsuÃ¡rio salvo com sucesso\"}")));
    }

    @Test
    public void saveUserByPUT() throws Exception {

        User user = new User();
        user.setBirthDate(LocalDate.now());
        user.setCpf("12354896575");
        user.setEmail("nome@site.com");
        user.setName("xpto");
        user.setId(1L);

        Mockito.when(userService.save(Mockito.any())).thenReturn(user);

        String jsonUser = UserRegistrationControllTest.mapper.writeValueAsString(user);

        mockMvc.perform(MockMvcRequestBuilders.put("/update-user").content(jsonUser).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("{\"message\":\"UsuÃ¡rio atualizado com sucesso\"}")));
    }

    @Test
    public void listUsersEmpty() throws Exception {

        List<User> list = List.of();

        Mockito.when(userService.listAllUsersOrderedByNameAscending()).thenReturn(list);

        String result = UserRegistrationControllTest.mapper.writeValueAsString(list);

        mockMvc.perform(MockMvcRequestBuilders.get("/list-users")).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(result)));
    }

    @Test
    public void listUsers() throws Exception {

        User user = new User();
        user.setBirthDate(LocalDate.now());
        user.setCpf("12354896575");
        user.setEmail("nome@site.com");
        user.setName("xpto");
        user.setId(1L);

        List<User> list = List.of(user);

        Mockito.when(userService.listAllUsersOrderedByNameAscending()).thenReturn(list);

        String result = UserRegistrationControllTest.mapper.writeValueAsString(list);

        mockMvc.perform(MockMvcRequestBuilders.get("/list-users")).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(result)));
    }
}