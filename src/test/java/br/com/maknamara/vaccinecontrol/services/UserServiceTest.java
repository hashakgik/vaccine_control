package br.com.maknamara.vaccinecontrol.services;

import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.maknamara.vaccinecontrol.exceptions.DeletionNotAllowedException;
import br.com.maknamara.vaccinecontrol.exceptions.RecordsConflictException;
import br.com.maknamara.vaccinecontrol.model.User;
import br.com.maknamara.vaccinecontrol.repository.UserRepository;
import br.com.maknamara.vaccinecontrol.service.UserService;
import br.com.maknamara.vaccinecontrol.service.VaccinationService;

@SpringBootTest(classes = { UserService.class })
@ExtendWith(SpringExtension.class)
public class UserServiceTest {

    private static final String EXCLUSÃO_NÃO_PERMITIDA_USUÁRIO_JÁ_ESTÁ_ASSOCIADO_À_VACINAÇÃO = "Exclusão não permitida. Usuário já está associado à vacinação";

    private static final String DEVERIA_DISPARAR_EXCEÇÃO = "Deveria disparar exceção";

    private static final String EMAIL_E_OU_CPF_JÁ_CADASTRADO_NO_SISTEMA = "Email e/ou cpf já cadastrado no sistema";

    @Autowired
    private UserService userService;

    @MockBean
    private VaccinationService vaccinationService;

    @MockBean
    private UserRepository userRepository;

    @BeforeAll
    public static void beforeAll() {
    }

    @Test
    public void saveWithoutSuccess() throws RecordsConflictException {

        User previousllySavedUser = new User();

        previousllySavedUser.setEmail("email");
        previousllySavedUser.setCpf("12354698741");

        List<User> expectedList = new ArrayList<>();
        expectedList.add(new User());

        Mockito.when(userRepository.findByEmailOrCpf(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(expectedList);

        try {
            userService.save(previousllySavedUser);
            Assertions.fail(DEVERIA_DISPARAR_EXCEÇÃO);
        } catch (RecordsConflictException e) {
            Assertions.assertThat(e.getLocalizedMessage()).isEqualTo(EMAIL_E_OU_CPF_JÁ_CADASTRADO_NO_SISTEMA);
        }
        Mockito.verify(userRepository, times(1)).findByEmailOrCpf(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void updateWithoutSuccess() {

        User previousllySavedUser = new User();

        previousllySavedUser.setId(1L);
        previousllySavedUser.setEmail("email");
        previousllySavedUser.setCpf("12354698741");

        List<User> expectedList = new ArrayList<>();
        expectedList.add(new User());

        Mockito.when(userRepository.findNotMeByEmailAndCpf(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(expectedList);
        try {
            userService.save(previousllySavedUser);
            Assertions.fail(DEVERIA_DISPARAR_EXCEÇÃO);
        } catch (RecordsConflictException e) {
            Assertions.assertThat(e.getLocalizedMessage()).isEqualTo(EMAIL_E_OU_CPF_JÁ_CADASTRADO_NO_SISTEMA);
        }
        Mockito.verify(userRepository, times(1)).findNotMeByEmailAndCpf(Mockito.anyLong(), Mockito.anyString(),
                Mockito.anyString());
    }

    @Test
    public void updateWithSuccess() throws RecordsConflictException {

        User previousllySavedUser = new User();
        previousllySavedUser.setId(1L);

        User expectedSavedUser = new User();
        expectedSavedUser.setId(1L);

        Mockito.when(userRepository.findNotMeByEmailAndCpf(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(List.of(new User()));

        Mockito.when(userRepository.save(Mockito.any())).thenReturn(expectedSavedUser);

        User savedUser = userService.save(previousllySavedUser);
        Assertions.assertThat(savedUser.getId()).isEqualTo(expectedSavedUser.getId());
    }

    @Test
    public void saveWithSuccess() throws RecordsConflictException {

        User unsavedUser = new User();
        User expectedSavedUser = new User();
        expectedSavedUser.setId(1L);

        Mockito.when(userRepository.findByEmailOrCpf(Mockito.anyString(), Mockito.anyString())).thenReturn(List.of());
        Mockito.when(userRepository.save(Mockito.any())).thenReturn(expectedSavedUser);
        User savedUser = userService.save(unsavedUser);
        Assertions.assertThat(savedUser.getId()).isEqualTo(expectedSavedUser.getId());
    }

    @Test
    public void deleteByIdWithSuccess() throws DeletionNotAllowedException {
        Mockito.doNothing().when(userRepository).deleteById(Mockito.anyLong());
        Mockito.when(vaccinationService.hasVaccinationaAssociatedWith(Mockito.anyLong())).thenReturn(false);

        userService.deleteById(1L);

        Mockito.verify(userRepository, times(1)).deleteById(Mockito.anyLong());
        Mockito.verify(vaccinationService, times(1)).hasVaccinationaAssociatedWith(Mockito.anyLong());
    }

    @Test
    public void deleteByIdWithoutSuccess() {
        Mockito.doNothing().when(userRepository).deleteById(Mockito.anyLong());
        Mockito.when(vaccinationService.hasVaccinationaAssociatedWith(Mockito.anyLong())).thenReturn(true);

        try {
            userService.deleteById(1L);
            Assertions.fail(DEVERIA_DISPARAR_EXCEÇÃO);
        } catch (DeletionNotAllowedException e) {
            Assertions.assertThat(e.getLocalizedMessage())
                    .isEqualTo(EXCLUSÃO_NÃO_PERMITIDA_USUÁRIO_JÁ_ESTÁ_ASSOCIADO_À_VACINAÇÃO);
        }

        Mockito.verify(userRepository, times(0)).deleteById(Mockito.anyLong());
        Mockito.verify(vaccinationService, times(1)).hasVaccinationaAssociatedWith(Mockito.anyLong());
    }

    @Test
    public void listAllUsersOrderedByNameAscendingNotEmpty() throws Exception {

        User user = new User();

        List<User> expectedList = List.of(user);

        Mockito.when(userRepository.findByOrderByNameAsc()).thenReturn(expectedList);

        List<User> list = userService.listAllUsersOrderedByNameAscending();
        Assertions.assertThat(list).contains(user);
    }

    @Test
    public void listAllUsersOrderedByNameAscendingEmpty() throws Exception {
        List<User> list = userService.listAllUsersOrderedByNameAscending();
        Assertions.assertThat(list).isEmpty();
    }
}