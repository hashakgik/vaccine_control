package br.com.maknamara.vaccinecontrol.services;

import static org.mockito.Mockito.times;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.maknamara.vaccinecontrol.exceptions.RecordsConflictException;
import br.com.maknamara.vaccinecontrol.model.Vaccination;
import br.com.maknamara.vaccinecontrol.repository.VaccinationRepository;
import br.com.maknamara.vaccinecontrol.service.VaccinationService;

@SpringBootTest(classes = { VaccinationService.class })
@ExtendWith(SpringExtension.class)
public class VaccinationServiceTest {

    @Autowired
    private VaccinationService vaccinationrService;

    @MockBean
    private VaccinationRepository vaccinationrRepository;

    @BeforeAll
    public static void beforeAll() {
    }

    @Test
    public void hasNotVaccinationaAssociatedWith() {

        Mockito.when(vaccinationrRepository.hasVaccinationaAssociatedWith(Mockito.any())).thenReturn(false);

        boolean result = vaccinationrService.hasVaccinationaAssociatedWith(1L);

        Assertions.assertThat(result).isFalse();

        Mockito.verify(vaccinationrRepository, times(1)).hasVaccinationaAssociatedWith(Mockito.any());
    }

    @Test
    public void hasVaccinationaAssociatedWith() {

        Mockito.when(vaccinationrRepository.hasVaccinationaAssociatedWith(Mockito.any())).thenReturn(true);

        boolean result = vaccinationrService.hasVaccinationaAssociatedWith(1L);

        Assertions.assertThat(result).isTrue();

        Mockito.verify(vaccinationrRepository, times(1)).hasVaccinationaAssociatedWith(Mockito.any());
    }

    @Test
    public void updateWithSuccess() throws RecordsConflictException {

        Vaccination previousllySavedVaccination = new Vaccination();
        previousllySavedVaccination.setId(1L);

        Vaccination expectedSavedVaccination = new Vaccination();
        expectedSavedVaccination.setId(1L);

        Mockito.when(vaccinationrRepository.save(Mockito.any())).thenReturn(expectedSavedVaccination);

        Vaccination savedVaccination = vaccinationrService.save(previousllySavedVaccination);

        Assertions.assertThat(savedVaccination.getId()).isEqualTo(expectedSavedVaccination.getId());

        Mockito.verify(vaccinationrRepository, times(1)).save(Mockito.any());
    }

    @Test
    public void saveWithSuccess() throws RecordsConflictException {

        Vaccination unsavedVaccination = new Vaccination();
        Vaccination expectedSavedVaccination = new Vaccination();
        expectedSavedVaccination.setId(1L);

        Mockito.when(vaccinationrRepository.save(Mockito.any())).thenReturn(expectedSavedVaccination);
        Vaccination savedVaccination = vaccinationrService.save(unsavedVaccination);
        Assertions.assertThat(savedVaccination.getId()).isEqualTo(expectedSavedVaccination.getId());
    }

    @Test
    public void deleteById() {
        Mockito.doNothing().when(vaccinationrRepository).deleteById(Mockito.anyLong());
        vaccinationrService.deleteById(1L);
    }

    @Test
    public void listAllVaccinationsOrderedByNameAscendingNotEmpty() throws Exception {

        Vaccination vaccinationr = new Vaccination();

        List<Vaccination> expectedList = List.of(vaccinationr);

        Mockito.when(vaccinationrRepository.findByOrderByNameAsc()).thenReturn(expectedList);

        List<Vaccination> list = vaccinationrService.listAllVaccinationsOrderedByNameAscending();
        Assertions.assertThat(list).contains(vaccinationr);
    }

    @Test
    public void listAllVaccinationsOrderedByNameAscendingEmpty() throws Exception {
        List<Vaccination> list = vaccinationrService.listAllVaccinationsOrderedByNameAscending();
        Assertions.assertThat(list).isEmpty();
    }
}