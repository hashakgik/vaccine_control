package br.com.maknamara.vaccinecontrol;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaccineControl {

    public static void main(String[] args) {
        SpringApplication.run(VaccineControl.class, args);
        Banner.Mode.OFF.getClass();
    }
}
