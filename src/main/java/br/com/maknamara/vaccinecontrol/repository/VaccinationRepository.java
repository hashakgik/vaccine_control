package br.com.maknamara.vaccinecontrol.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.maknamara.vaccinecontrol.model.Vaccination;

@Repository
public interface VaccinationRepository extends JpaRepository<Vaccination, Long> {

    List<Vaccination> findByOrderByNameAsc();

    @Query(value = "SELECT count(v) > 0 FROM Vaccination v JOIN v.user u WHERE u.id = ?1")
    boolean hasVaccinationaAssociatedWith(Long id);

}
