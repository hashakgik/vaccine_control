package br.com.maknamara.vaccinecontrol.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.maknamara.vaccinecontrol.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByEmailOrCpf(String email, String cpf);

    @Query("SELECT u FROM User u WHERE u.id <> ?1 AND (u.email = ?2 OR u.cpf = ?3)")
    List<User> findNotMeByEmailAndCpf(Long id, String email, String cpf);

    List<User> findByOrderByNameAsc();
}
