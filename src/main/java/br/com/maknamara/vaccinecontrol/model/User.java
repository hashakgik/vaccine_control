package br.com.maknamara.vaccinecontrol.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "USER", uniqueConstraints = { @UniqueConstraint(name = "UNK_EMAIL", columnNames = { "email" }),
        @UniqueConstraint(name = "UNK_CPF", columnNames = { "cpf" }) })
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    @NotBlank(message = "O campo \"Nome\" é obrigatório")
    @Size(max = 60, message = "O tamanho máximo do campo \"Nome\" é de 60 caracteres")
    private String name;

    @NotBlank(message = "O campo \"Email\" é obrigatório")
    @Size(max = 60, message = "O tamanho máximo do campo \"Email\" é de 60 caracteres")
    @Email(message = "O \"Email\" está incorreto")
    private String email;

    @NotBlank(message = "O campo \"CPF\" é obrigatório")
    @Size(min = 11, max = 11, message = "O tamanho do campo \"CPF\" é de 11 digitos")
    private String cpf;

    @NotNull(message = "O campo \"Data nascimento\" é obrigatório")
    private LocalDate birthDate;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Vaccination> vaccinations = new ArrayList<>(0);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public List<Vaccination> getVaccinations() {
        return vaccinations;
    }

    public void setVaccinations(List<Vaccination> vaccinations) {
        this.vaccinations = vaccinations;
    }

    @Override
    public String toString() {
        return String.format("User [name=%s, email=%s, cpf=%s, birthDate=%s]", name, email, cpf, birthDate);
    }

}
