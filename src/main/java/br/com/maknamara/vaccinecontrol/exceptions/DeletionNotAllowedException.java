package br.com.maknamara.vaccinecontrol.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
public class DeletionNotAllowedException extends Exception {
    public DeletionNotAllowedException() {
        this("Exclusão não permitida. Usuário já está associado à vacinação");
    }

    public DeletionNotAllowedException(String message) {
        super(message);
    }

}
