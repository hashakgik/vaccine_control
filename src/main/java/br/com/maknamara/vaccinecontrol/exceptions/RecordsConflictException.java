package br.com.maknamara.vaccinecontrol.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.CONFLICT)
public class RecordsConflictException extends Exception {

    public RecordsConflictException() {
        this("Email e/ou cpf já cadastrado no sistema");
    }

    public RecordsConflictException(String message) {
        super(message);
    }
}
