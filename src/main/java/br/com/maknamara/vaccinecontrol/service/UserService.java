package br.com.maknamara.vaccinecontrol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.maknamara.vaccinecontrol.exceptions.DeletionNotAllowedException;
import br.com.maknamara.vaccinecontrol.exceptions.RecordsConflictException;
import br.com.maknamara.vaccinecontrol.model.User;
import br.com.maknamara.vaccinecontrol.repository.UserRepository;

@Transactional
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VaccinationService vaccinationService;

    public List<User> listAllUsersOrderedByNameAscending() {
        return userRepository.findByOrderByNameAsc();
    }

    public User save(User user) throws RecordsConflictException {
        List<User> conflictedRecords;
        if (user.getId() == null) {
            conflictedRecords = userRepository.findByEmailOrCpf(user.getEmail(), user.getCpf());
        } else {
            conflictedRecords = userRepository.findNotMeByEmailAndCpf(user.getId(), user.getEmail(), user.getCpf());
        }
        if (!conflictedRecords.isEmpty()) {
            throw new RecordsConflictException();
        }
        return userRepository.save(user);
    }

    public void deleteById(Long id) throws DeletionNotAllowedException {
        if (vaccinationService.hasVaccinationaAssociatedWith(id)) {
            throw new DeletionNotAllowedException();
        }
        userRepository.deleteById(id);
    }
}
