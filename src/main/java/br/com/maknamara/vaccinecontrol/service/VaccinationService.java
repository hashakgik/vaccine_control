package br.com.maknamara.vaccinecontrol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.maknamara.vaccinecontrol.model.Vaccination;
import br.com.maknamara.vaccinecontrol.repository.VaccinationRepository;

@Transactional
@Service
public class VaccinationService {
    @Autowired
    private VaccinationRepository vaccinationRepository;

    public List<Vaccination> listAllVaccinationsOrderedByNameAscending() {
        return vaccinationRepository.findByOrderByNameAsc();
    }

    public Vaccination save(Vaccination vaccination) {
        return vaccinationRepository.save(vaccination);
    }

    public void deleteById(Long id) {
        vaccinationRepository.deleteById(id);
    }

    public boolean hasVaccinationaAssociatedWith(Long id) {
        return vaccinationRepository.hasVaccinationaAssociatedWith(id);
    }
}
