package br.com.maknamara.vaccinecontrol.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.maknamara.vaccinecontrol.exceptions.DeletionNotAllowedException;
import br.com.maknamara.vaccinecontrol.exceptions.RecordsConflictException;
import br.com.maknamara.vaccinecontrol.model.User;
import br.com.maknamara.vaccinecontrol.service.UserService;

@RestController
public class UserRegistrationControll {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/list-users", produces = "application/json")
    @ResponseBody
    public ResponseEntity<List<User>> listUsers() {
        List<User> list = userService.listAllUsersOrderedByNameAscending();
        return new ResponseEntity<List<User>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping(path = "/save-user", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> saveUser(@Valid @RequestBody User user) throws RecordsConflictException {
        user = userService.save(user);
        return new ResponseEntity<String>("{\"message\":\"Usuário salvo com sucesso\"}", new HttpHeaders(),
                HttpStatus.CREATED);
    }

    @PutMapping(path = "/update-user", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> updateUser(@Valid @RequestBody User user) throws RecordsConflictException {
        user = userService.save(user);
        return new ResponseEntity<String>("{\"message\":\"Usuário atualizado com sucesso\"}", new HttpHeaders(),
                HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete-user/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> deleteUserById(@PathVariable("id") Long id) throws DeletionNotAllowedException {
        userService.deleteById(id);
        return new ResponseEntity<String>("{\"message\":\"Usuário excluído com sucesso\"}", new HttpHeaders(),
                HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(DeletionNotAllowedException.class)
    public Map<String, String> handleDeletionNotAllowedException(DeletionNotAllowedException ex) {
        Map<String, String> errors = new HashMap<>();
        String errorMessage = ex.getLocalizedMessage();
        errors.put("message", errorMessage);
        return errors;
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(RecordsConflictException.class)
    public Map<String, String> handleRecordsConflictException(RecordsConflictException ex) {
        Map<String, String> errors = new HashMap<>();
        String errorMessage = ex.getLocalizedMessage();
        errors.put("message", errorMessage);
        return errors;
    }
}
