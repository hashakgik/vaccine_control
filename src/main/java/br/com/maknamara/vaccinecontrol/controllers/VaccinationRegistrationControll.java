package br.com.maknamara.vaccinecontrol.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.maknamara.vaccinecontrol.exceptions.DeletionNotAllowedException;
import br.com.maknamara.vaccinecontrol.exceptions.RecordsConflictException;
import br.com.maknamara.vaccinecontrol.model.Vaccination;
import br.com.maknamara.vaccinecontrol.service.VaccinationService;

@RestController
public class VaccinationRegistrationControll {

    @Autowired
    private VaccinationService vaccinationService;

    @GetMapping(path = "/list-vaccinations", produces = "application/json")
    public ResponseEntity<List<Vaccination>> listVaccinations() {
        List<Vaccination> list = vaccinationService.listAllVaccinationsOrderedByNameAscending();
        return new ResponseEntity<List<Vaccination>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping(path = "/save-vaccination", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> saveVaccination(@Valid @RequestBody Vaccination vaccination)
            throws RecordsConflictException {
        vaccination = vaccinationService.save(vaccination);
        return new ResponseEntity<String>("{\"message\":\"Vacinação salva com sucesso\"}", new HttpHeaders(),
                HttpStatus.CREATED);
    }

    @PutMapping(path = "/update-vaccination", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> updateVaccination(@Valid @RequestBody Vaccination vaccination)
            throws RecordsConflictException {
        vaccination = vaccinationService.save(vaccination);
        return new ResponseEntity<String>("{\"message\":\"Vacinação atualizada com sucesso\"}", new HttpHeaders(),
                HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete-vaccination/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> deleteVaccinationById(@PathVariable("id") Long id)
            throws DeletionNotAllowedException {
        vaccinationService.deleteById(id);
        return new ResponseEntity<String>("{\"message\":\"Vacinação excluída com sucesso\"}", new HttpHeaders(),
                HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
