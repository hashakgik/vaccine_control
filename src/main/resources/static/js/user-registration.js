function edit(user) {
    $('#id').val(user.id);
    $('#name').val(user.name);
    $('#email').val(user.email);
    $('#cpf').val(user.cpf);
    $('#birthDate').val(user.birthDate);
}

function clearAlertMessages() {
    $('.container .alert').each((_index, element) => element.remove());
}

function showMessages(object, type) {
    Object.keys(object).forEach(key => {
        $('.container').prepend(`<div class="alert alert-${type} alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Atenção!</strong> ${object[key]}</div>`);
    });
}

function remove(row, id) {
    clearAlertMessages();
    $.ajax({
        type: 'DELETE',
        url: '/delete-user/' + id,
        success: (response, _status, _xhr) => {
            row.remove();
            showMessages(response, 'info');
        },
        error: (response, _status, _error) => {
            let err = JSON.parse(response.responseText);
            showMessages(err, 'warning');
        },
        contentType: "application/json"
    });
}

addEventListener("load", _e => {
    $('#list').on('click', () => {
        clearAlertMessages();
        let panelListUser = $('#panelListUser')[0];

        $.ajax({
            type: 'GET',
            url: '/list-users/',
            success: (response, _status, _xhr) => {
                panelListUser.querySelectorAll('tbody tr').forEach(tr => tr.remove());

                response.forEach(user => {
                    let row = panelListUser.querySelector('tbody').insertRow(-1);

                    row.insertCell(-1).innerHTML = user.id;
                    row.insertCell(-1).innerHTML = user.name;
                    row.insertCell(-1).innerHTML = user.email;
                    row.insertCell(-1).innerHTML = user.cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4');
                    row.insertCell(-1).innerHTML = new Date(user.birthDate + 'T00:00:00.000').toLocaleDateString("pt-BR");

                    let btn = document.createElement('button');

                    btn.innerHTML = '<span class="glyphicon glyphicon-edit">';
                    btn.type = 'button';
                    btn.className = 'btn btn-default btn-sm';
                    btn.addEventListener('click', edit.bind(null, user));

                    row.insertCell(-1).appendChild(btn);

                    row.insertCell(-1).innerHTML = `<button onclick='remove(this.parentNode.parentNode,${user.id})' type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-trash"></span>`;

                });
            },
            contentType: "application/json"
        });
    });

    $('#save').on('click', () => {

        clearAlertMessages();

        let user = {};

        user.id = $('#id').val();
        user.name = $('#name').val();
        user.email = $('#email').val();
        user.cpf = $('#cpf').val();
        user.birthDate = $('#birthDate').val();

        $.ajax({
            type: user.id === '' ? 'POST' : 'PUT',
            url: user.id === '' ? '/save-user' : 'update-user',
            data: JSON.stringify(user),
            success: (response, _status, _xhr) => {

                $('#id').val('');
                $('#name').val('');
                $('#email').val('');
                $('#cpf').val('');
                $('#birthDate').val('');

                $('#list').trigger('click');
                
                showMessages(response, 'info');
            },
            error: (response, _status, _error) => {
                let err = JSON.parse(response.responseText);
                showMessages(err, 'warning');
            },
            contentType: "application/json"
        });

    });
});