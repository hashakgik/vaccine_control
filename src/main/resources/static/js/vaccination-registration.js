function clearAlertMessages() {
    $('.container .alert').each((_index, element) => element.remove());
}

function showMessages(object, type) {
    Object.keys(object).forEach(key => {
        $('.container').prepend(`<div class="alert alert-${type} alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Atenção!</strong> ${object[key]}</div>`);
    });
}
function edit(vaccination) {
    $('#id').val(vaccination.id);
    $('#name').val(vaccination.name);
    $('#user').val(vaccination.user.id);
    $('#vaccinationDate').val(vaccination.vaccinationDate);
}

function remove(row, id) {
    clearAlertMessages();
    $.ajax({
        type: 'DELETE',
        url: '/delete-vaccination/' + id,
        success: (response, _status, _xhr) => {
            row.remove();
            showMessages(response, 'info');
        },
        contentType: "application/json"
    });
}

addEventListener("load", _e => {
    $.get("/list-users", data => {

        $('select').children().remove().end();
        $('select').append(`<option></option>`);

        data.forEach(user => {
            $('select').append(`<option value='${user.id}'>${user.name + '-'.repeat(60 - user.name.length)}|${user.cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4')}</option>`);
        });
    });

    $('#list').on('click', () => {
        clearAlertMessages();
        let panelListVaccination = $('#panelListVaccination')[0];
        $.get("/list-vaccinations", (response, _status, _xhr) => {

            panelListVaccination.querySelectorAll('tbody tr').forEach(tr => tr.remove());

            response.forEach(vaccination => {
                let row = panelListVaccination.querySelector('tbody').insertRow(-1);

                row.insertCell(-1).innerHTML = vaccination.id;
                row.insertCell(-1).innerHTML = vaccination.name;
                row.insertCell(-1).innerHTML = vaccination.user.name;
                row.insertCell(-1).innerHTML = new Date(vaccination.vaccinationDate + 'T00:00:00.000').toLocaleDateString("pt-BR");

                let btn = document.createElement('button');

                btn.innerHTML = '<span class="glyphicon glyphicon-edit">';
                btn.type = 'button';
                btn.className = 'btn btn-default btn-sm';
                btn.addEventListener('click', edit.bind(null, vaccination));

                row.insertCell(-1).appendChild(btn);

                row.insertCell(-1).innerHTML = `<button onclick='remove(this.parentNode.parentNode,${vaccination.id})' type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-trash"></span>`;

            });
        });
    });

    $('#save').on('click', () => {

         clearAlertMessages();

        let vaccination = { user: {} };

        vaccination.id = $('#id').val();
        vaccination.name = $('#name').val();
        vaccination.user.id = $('#user').val();
        vaccination.user.name = $('#email').text();

        if (vaccination.user.id === '') {
            vaccination.user = null;
        }

        vaccination.vaccinationDate = $('#vaccinationDate').val();

        $.ajax({
            type: vaccination.id === '' ? 'POST' : 'PUT',
            url: vaccination.id === '' ? '/save-vaccination' : 'update-vaccination',
            data: JSON.stringify(vaccination),
            success: (response, _status, _xhr) => {

                $('#id').val('');
                $('#name').val('');
                $('#user').val('');
                $('#vaccinationDate').val('');

                $('#list').trigger('click');
                
                showMessages(response, 'info');
            },
            error: (response, _status, _error) => {
                let err = JSON.parse(response.responseText);                
                showMessages(err, 'warning');
            },
            contentType: "application/json"
        });

    });
});